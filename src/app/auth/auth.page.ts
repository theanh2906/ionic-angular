import { Component, OnInit } from '@angular/core';
import { AuthResponseData, AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLoading = false;
  isLogin = true;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  authenticate(email: string, password: string) {
    this.isLoading = true;
    this.loadingController
      .create({
        keyboardClose: true,
        spinner: 'circular',
        message: 'Logging in ...',
      })
      .then((loadingEl) => {
        loadingEl.present();
        let authObs: Observable<AuthResponseData>;
        if (this.isLogin) {
          authObs = this.authService.login(email, password);
        } else {
          authObs = this.authService.signup(email, password);
        }
        authObs.subscribe(
          (resData) => {
            this.isLoading = false;
            loadingEl.dismiss();
            this.router.navigateByUrl('/');
          },
          (errorRes) => {
            loadingEl.dismiss();
            const code = errorRes.error.error.message;
            let message = 'Could not create account, Please try again. ';
            switch (code) {
              case 'EMAIL_EXISTS':
                message = 'This email already exists!';
                break;
              case 'EMAIL_NOT_FOUND':
                message = 'Email address could not be found.';
                break;
              case 'INVALID_PASSWORD':
                message = 'This password is not correct.';
                break;
            }
            this.showErrorAlert(message);
          }
        );
      });
  }

  onSubmit = (loginForm: NgForm) => {
    if (!loginForm.valid) {
      return;
    }
    const email = loginForm.value.email;
    const password = loginForm.value.password;
    this.authenticate(email, password);
    loginForm.reset();
  };

  onSwitchAuthMode = () => {
    this.isLogin = !this.isLogin;
  };

  private showErrorAlert = (message: string) => {
    this.alertController
      .create({
        header: 'Authentication failed',
        message,
        buttons: ['Ok'],
      })
      .then((alertEl) => alertEl.present());
  };
}
