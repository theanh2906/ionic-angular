import { Component, OnDestroy, OnInit } from '@angular/core';
import { Booking, BookingsService } from 'src/app/bookings/bookings.service';
import { Subscription } from 'rxjs';
import { IonItemSliding, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit, OnDestroy {
  bookings: Booking[];
  bookingsSub: Subscription;
  isLoading = false;
  constructor(
    private bookingsService: BookingsService,
    private loadingController: LoadingController
  ) {}
  onCancel(id: string, slidingEl: IonItemSliding) {
    this.loadingController
      .create({
        message: 'Cancelling booking...',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.bookingsService.cancelBooking(id).subscribe(() => {
          loadingEl.dismiss();
        });
        slidingEl.close();
      });
  }

  ngOnInit(): void {
    this.bookingsSub = this.bookingsService.bookings.subscribe((bookings) => {
      this.bookings = bookings;
    });
  }
  ionViewWillEnter() {
    this.isLoading = true;
    this.bookingsService.fetchBookings().subscribe(() => {
      this.isLoading = false;
    });
  }
  ngOnDestroy() {
    if (this.bookingsSub) {
      this.bookingsSub.unsubscribe();
    }
  }
}
