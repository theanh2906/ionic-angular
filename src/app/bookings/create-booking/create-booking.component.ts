import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Place } from 'src/app/places/places.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.scss'],
})
export class CreateBookingComponent implements OnInit {
  @Input() place: Place;
  @Input() mode: 'select' | 'random';
  @ViewChild('bookForm', { static: true }) bookForm: NgForm;
  startDate: string;
  endDate: string;
  constructor(private modalController: ModalController) {}

  ngOnInit() {
    const availableFrom = new Date(this.place.availableFrom);
    const availableTo = new Date(this.place.availableTo);
    if (this.mode === 'random') {
      this.startDate = new Date(
        availableFrom.getTime() +
          Math.random() *
            (availableTo.getTime() -
              7 * 24 * 60 * 60 * 1000 -
              availableFrom.getTime())
      ).toISOString();
      this.endDate = new Date(
        new Date(this.startDate).getTime() +
          Math.random() *
            (new Date(this.startDate).getTime() +
              6 * 24 * 60 * 60 * 1000 -
              new Date(this.startDate).getTime())
      ).toISOString();
    }
  }

  onBookPlace() {
    if (!this.bookForm.valid || !this.datesValid) {
      return;
    }
    this.modalController.dismiss(
      {
        bookingData: {
          firstName: this.bookForm.value['first-name'],
          lastName: this.bookForm.value['last-name'],
          guestNumber: +this.bookForm.value['guest-number'],
          dateFrom: new Date(this.bookForm.value['date-from']),
          dateTo: new Date(this.bookForm.value['date-to']),
          placeId: this.place.id,
          placeTitle: this.place.title,
        },
      },
      'confirm'
    );
  }

  onCancel() {
    this.modalController.dismiss(null, 'cancel', 'bookModal');
  }

  datesValid = () => {
    const startDate = new Date(this.bookForm.value['date-from']);
    const endDate = new Date(this.bookForm.value['date-to']);
    return startDate.getTime() <= endDate.getTime();
  };
}
