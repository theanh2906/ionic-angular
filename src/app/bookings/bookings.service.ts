import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { HttpClient } from '@angular/common/http';

export interface Booking {
  id: string;
  placeId: string;
  userId: string;
  placeTitle: string;
  placeImage: string;
  firstName: string;
  lastName: string;
  guestNumber: number;
  dateFrom: Date;
  dateTo: Date;
}

interface BookingData {
  dateFrom: string;
  dateTo: string;
  firstName: string;
  guestNumber: number;
  lastName: string;
  placeId: string;
  placeImage: string;
  placeTitle: string;
  userId: string;
}

@Injectable({
  providedIn: 'root',
})
export class BookingsService {
  constructor(private authService: AuthService, private http: HttpClient) {}

  private _bookings = new BehaviorSubject<Booking[]>([]);

  get bookings() {
    return this._bookings.asObservable();
  }

  deleteById = (id: string) => [
    // ...this._bookings.filter((value) => value.id !== id),
  ];

  fetchBookings = () => {
    let fetchedToken: string;
    return this.authService.token.pipe(
      take(1),
      switchMap((token) => {
        fetchedToken = token;
        return this.authService.userId;
      }),
      take(1),
      switchMap((userId) => {
        if (!userId) {
          throw new Error('User not found!');
        }
        return this.http.get<{ [key: string]: BookingData }>(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/bookings.json?auth=${fetchedToken}&` +
            `orderBy="userId"&equalTo="${userId}"`
        );
      }),
      map((resData: any) => {
        const bookings: Booking[] = [];
        for (const key in resData) {
          if (resData.hasOwnProperty(key)) {
            bookings.push({
              id: key,
              placeId: resData[key].placeId,
              placeTitle: resData[key].placeTitle,
              dateTo: new Date(resData[key].dateTo),
              dateFrom: new Date(resData[key].dateFrom),
              lastName: resData[key].lastName,
              userId: resData[key].userId,
              placeImage: resData[key].placeImage,
              firstName: resData[key].firstName,
              guestNumber: resData[key].guestNumber,
            });
          }
        }
        return bookings;
      }),
      tap((bookings) => {
        this._bookings.next(bookings);
      })
    );
  };

  addBooking = (booking: {
    firstName: string;
    lastName: string;
    guestNumber: number;
    placeId: string;
    placeTitle: string;
    dateFrom: Date;
    dateTo: Date;
  }) => {
    let generatedId;
    let newBooking: Booking;
    let fetchedUserId: string;
    return this.authService.userId.pipe(
      take(1),
      switchMap((userId) => {
        if (!userId) {
          throw new Error('No user id found!');
        }
        fetchedUserId = userId;
        return this.authService.token;
      }),
      take(1),
      switchMap((token) => {
        newBooking = {
          id: Math.random().toString(),
          userId: fetchedUserId,
          placeImage:
            'https://cache.marriott.com/marriottassets/marriott/CAIBR/caibr-exterior-0044-hor-feat.jpg',
          guestNumber: booking.guestNumber,
          lastName: booking.lastName,
          firstName: booking.firstName,
          dateTo: booking.dateTo,
          dateFrom: booking.dateFrom,
          placeTitle: booking.placeTitle,
          placeId: booking.placeId,
        };
        return this.http.post<{ name: string }>(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/bookings.json?auth=${token}`,
          { ...newBooking, id: null }
        );
      }),
      switchMap((resData) => {
        generatedId = resData.name;
        return this._bookings;
      }),
      take(1),
      tap((bookings) => {
        newBooking.id = generatedId;
        this._bookings.next(bookings.concat(newBooking));
      })
    );
  };

  cancelBooking = (id: string) =>
    this.authService.token.pipe(
      take(1),
      switchMap((token) => {
        return this.http.delete(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/bookings/${id}.json?auth=${token}`
        );
      }),
      switchMap(() => this.bookings),
      take(1),
      tap((bookings) => {
        this._bookings.next(bookings.filter((booking) => booking.id !== id));
      })
    );
}
