import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  ActionSheetController,
  AlertController,
  ModalController,
} from '@ionic/angular';
import { MapModalComponent } from 'src/app/shared/map-modal/map-modal.component';
import { environment } from 'src/environments/environment';
import { map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { Geolocation } from '@capacitor/geolocation';

export interface Coordinates {
  lat: number;
  lng: number;
}

export interface PlaceLocation extends Coordinates {
  address: string;
  imageUrl: string;
}

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {
  @Output() locationPick = new EventEmitter<PlaceLocation>();
  selectedLocationImage: string;
  isLoading = false;
  constructor(
    private modalController: ModalController,
    private http: HttpClient,
    private actionSheetController: ActionSheetController,
    private alertController: AlertController
  ) {}
  ngOnInit() {}

  onPickLocation() {
    this.actionSheetController
      .create({
        header: 'Please choose action',
        buttons: [
          {
            text: 'Auto-Locate',
            handler: this.locateUser,
          },
          {
            text: 'Pick on map',
            handler: this.openMap,
          },
          {
            text: 'Cancel',
            role: 'cancel',
          },
        ],
      })
      .then((actionSheetEl) => {
        actionSheetEl.present();
      });
  }

  private locateUser = () => {
    if (!Capacitor.isPluginAvailable('Geolocation')) {
      this.showErrorAlert();
      return;
    }
    this.isLoading = true;
    Geolocation.getCurrentPosition()
      .then((geoPosition) => {
        const { coords } = geoPosition;
        const currentCoords: Coordinates = {
          lat: coords.latitude,
          lng: coords.longitude,
        };
        this.createPlace(currentCoords);
        this.isLoading = false;
      })
      .catch((err) => {
        this.showErrorAlert();
        this.isLoading = false;
      });
  };

  private showErrorAlert = () => {
    this.alertController
      .create({
        header: 'Could not fetch location',
        message: 'Please use the map to pick location!',
        buttons: ['OK'],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  };

  private openMap = () => {
    this.modalController
      .create({
        id: 'locPick',
        component: MapModalComponent,
      })
      .then((modalEl) => {
        modalEl.onDidDismiss().then((modalData) => {
          if (!modalData.data) {
            return;
          }
          const coordinates: Coordinates = {
            lat: modalData.data.lat,
            lng: modalData.data.lng,
          };
          this.createPlace(coordinates);
        });
        modalEl.present();
      });
  };

  private createPlace = (coords: Coordinates) => {
    const pickedLocation: PlaceLocation = {
      lat: coords.lat,
      lng: coords.lng,
      address: null,
      imageUrl: null,
    };
    this.isLoading = true;
    this.getAddress(coords)
      .pipe(
        switchMap((address) => {
          pickedLocation.address = address;
          return of(this.getMapImage(pickedLocation, 18));
        })
      )
      .subscribe((imageUrl) => {
        pickedLocation.imageUrl = imageUrl;
        this.selectedLocationImage = imageUrl;
        this.isLoading = false;
        this.locationPick.emit(pickedLocation);
      });
  };

  private getAddress = (coords: Coordinates) =>
    this.http
      .get<any>(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${coords.lat},${coords.lng}&key=${environment.googleMapAPIKey}`
      )
      .pipe(
        map((geoData: any) => {
          if (!geoData || !geoData.results || geoData.results.length === 0) {
            return null;
          }
          return geoData.results[0].formatted_address;
        })
      );

  private getMapImage = (coords: Coordinates, zoomNum: number) =>
    `https://maps.googleapis.com/maps/api/staticmap?` +
    `center=${coords.lat},${coords.lng}&` +
    `zoom=${zoomNum}&` +
    `size=500x300` +
    `&maptype=roadmap&` +
    `markers=color:red%7Clabel:Place%7C${coords.lat},${coords.lng}&` +
    `key=${environment.googleMapAPIKey}`;
}
