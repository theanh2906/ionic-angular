import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { BehaviorSubject, of } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { PlaceLocation } from 'src/app/shared/pickers/location-picker/location-picker.component';

export interface Place {
  id: string;
  title: string;
  description: string;
  imageUrl: string;
  price: number;
  availableFrom: Date;
  availableTo: Date;
  userId: string;
  location: PlaceLocation;
}

interface PlaceData {
  availableFrom: string;
  availableTo: string;
  description: string;
  imageUrl: string;
  price: number;
  title: string;
  userId: string;
  location: PlaceLocation;
}

@Injectable({
  providedIn: 'root',
})
export class PlacesService {
  constructor(private authService: AuthService, private http: HttpClient) {}

  // eslint-disable-next-line @typescript-eslint/member-ordering
  private _places = new BehaviorSubject<Place[]>([]);

  get places() {
    return this._places.asObservable();
  }

  fetchPlaces = () =>
    this.authService.token.pipe(
      take(1),
      switchMap((token) => {
        if (!token) {
          throw new Error('Token does not exist!');
        }
        return this.http.get<{ [key: string]: PlaceData }>(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/offered-places.json?auth=${token}`
        );
      }),
      map((resData) => {
        const places: Place[] = [];
        for (const key in resData) {
          if (resData.hasOwnProperty(key)) {
            places.push({
              id: key,
              title: resData[key].title,
              imageUrl: resData[key].imageUrl,
              availableFrom: new Date(resData[key].availableFrom),
              availableTo: new Date(resData[key].availableTo),
              userId: resData[key].userId,
              price: resData[key].price,
              description: resData[key].description,
              location: resData[key].location,
            });
          }
        }
        return places;
      }),
      tap((places) => {
        this._places.next(places);
      })
    );
  getPlaceById = (id: string) => {
    return this.authService.token.pipe(
      take(1),
      switchMap((token) => {
        if (!token) {
          throw new Error('Token does not exist!');
        }
        return this.http.get<PlaceData>(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/offered-places/${id}.json?auth=${token}`
        );
      }),
      map((placeData) => {
        const place: Place = {
          id,
          title: placeData.title,
          description: placeData.description,
          imageUrl: placeData.imageUrl,
          availableFrom: new Date(placeData.availableFrom),
          availableTo: new Date(placeData.availableTo),
          price: placeData.price,
          userId: placeData.userId,
          location: placeData.location,
        };
        return place;
      })
    );
  };
  deletePlaceById = (id: string) =>
    this.authService.token.pipe(
      take(1),
      switchMap((token) =>
        this.http.delete(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/offered-places/${id}.json?auth=${token}`
        )
      ),
      switchMap(() => this.places),
      take(1),
      tap((bookings) =>
        this._places.next(bookings.filter((booking) => booking.id !== id))
      )
    );
  uploadImage = (image: File) => {
    const uploadData = new FormData();
    uploadData.append('image', image);
    return this.authService.token.pipe(
      take(1),
      switchMap((token) =>
        this.http.post<{ imageUrl: string; imagePath: string }>(
          `https://us-central1-ionic-angular-api.cloudfunctions.net/storeImage`,
          uploadData,
          // eslint-disable-next-line @typescript-eslint/naming-convention
          { headers: { Authorization: 'Bearer ' + token } }
        )
      )
    );
  };

  addPlace(
    title: string,
    description: string,
    price: number,
    dateFrom: Date,
    dateTo: Date,
    location: PlaceLocation,
    imageUrl
  ) {
    let generatedId: string;
    let newPlace: Place;
    let fetchedUserId: string;
    return this.authService.userId.pipe(
      take(1),
      switchMap((userId) => {
        if (!userId) {
          throw new Error('No user id found!');
        }
        fetchedUserId = userId;
        return this.authService.token;
      }),
      take(1),
      switchMap((token) => {
        if (!fetchedUserId) {
          throw new Error('No user id found!');
        }
        newPlace = {
          id: Math.random().toString(),
          title,
          description,
          imageUrl,
          price,
          availableFrom: dateFrom,
          availableTo: dateTo,
          userId: fetchedUserId,
          location,
        };
        return (
          // When posting to Firebase, id will be generated automatically
          // Should use spread operator in order to override the value of id (null)
          this.http.post<{ name: string }>(
            `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/offered-places.json?auth=${token}`,
            { ...newPlace, id: null }
          )
        );
      }),
      // Using tap in order to implement side effect of current observable so that the observable is not stopped.
      // If using subscribe() the observable will stop.
      switchMap((resData) => {
        generatedId = resData.name;
        return this.places;
      }),
      take(1),
      tap((places) => {
        newPlace.id = generatedId;
        this._places.next(places.concat(newPlace));
      })
    );
  }

  editPlace = (id: string, title: string, description: string) => {
    let updatedPlaces: Place[] = [];
    let fetchedToken: string;
    return this.authService.token.pipe(
      take(1),
      switchMap((token) => {
        fetchedToken = token;
        return this.places;
      }),
      take(1),
      switchMap((places) => {
        if (!places || places.length <= 0) {
          return this.fetchPlaces();
        } else {
          return of(places);
        }
      }),
      switchMap((places) => {
        const updatedPlaceIndex = places.findIndex((pl) => pl.id === id);
        updatedPlaces = [...places];
        const old = updatedPlaces[updatedPlaceIndex];
        updatedPlaces[updatedPlaceIndex] = {
          id: old.id,
          title,
          description,
          imageUrl: old.imageUrl,
          availableFrom: old.availableFrom,
          availableTo: old.availableTo,
          userId: old.userId,
          price: old.price,
          location: old.location,
        };
        return this.http.put(
          `https://ionic-angular-api-default-rtdb.asia-southeast1.firebasedatabase.app/offered-places/${id}.json?auth=${fetchedToken}`,
          { ...updatedPlaces[updatedPlaceIndex], id: null }
        );
      }),
      tap(() => {
        this._places.next(updatedPlaces);
      })
    );
  };
}
