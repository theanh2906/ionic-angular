import { Component, OnDestroy, OnInit } from '@angular/core';
import { Place, PlacesService } from 'src/app/places/places.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  AlertController,
  LoadingController,
  NavController,
} from '@ionic/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offer-bookings',
  templateUrl: './offer-bookings.page.html',
  styleUrls: ['./offer-bookings.page.scss'],
})
export class OfferBookingsPage implements OnInit, OnDestroy {
  place: Place;
  form: FormGroup;
  isEditMode = false;
  isLoading = false;
  private placeSub: Subscription;
  constructor(
    private route: ActivatedRoute,
    private placesService: PlacesService,
    private navController: NavController,
    private loadingController: LoadingController,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (!params.placeId) {
        this.navController.navigateBack('/places/tabs/offers');
      }
      this.isLoading = true;
      this.placeSub = this.placesService.getPlaceById(params.placeId).subscribe(
        (place) => {
          this.place = place;
          this.form = new FormGroup({
            title: new FormControl(this.place.title, {
              updateOn: 'blur',
              validators: [Validators.required],
            }),
            description: new FormControl(this.place.description, {
              updateOn: 'blur',
              validators: [Validators.required, Validators.maxLength(180)],
            }),
          });
          this.isLoading = false;
        },
        (error) => {
          this.alertController
            .create({
              header: 'An error occurred!',
              message: 'Place could not be fetch. Please try again later',
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    this.router.navigate(['/places/tabs/offers']);
                  },
                },
              ],
            })
            .then((alertEl) => {
              alertEl.present();
            });
        }
      );
    });
  }
  saveOrEdit = () => {
    if (this.isEditMode) {
      this.loadingController
        .create({
          message: 'Updating place...',
        })
        .then((loadingEl) => {
          loadingEl.present();
          this.placesService
            .editPlace(
              this.place.id,
              this.form.value.title,
              this.form.value.description
            )
            .subscribe(() => {
              loadingEl.dismiss();
              this.form.reset();
              this.navController.navigateBack(['/places/tabs/offers']);
            });
        });
      // this.place = this.placesService.getPlaceById(
      //   this.route.snapshot.params.placeId
      // );
    }
    this.isEditMode = !this.isEditMode;
  };

  ngOnDestroy(): void {
    if (this.placeSub) {
      this.placeSub.unsubscribe();
    }
  }
}
