import { Component, OnDestroy, OnInit } from '@angular/core';
import { Place, PlacesService } from 'src/app/places/places.service';
import { Router } from '@angular/router';
import {
  IonItemSliding,
  LoadingController,
  NavController,
} from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit, OnDestroy {
  offers: Place[];
  isLoading = false;
  private placeSub: Subscription;
  constructor(
    private placesService: PlacesService,
    private router: Router,
    private navController: NavController,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.placeSub = this.placesService.places.subscribe((places) => {
      this.offers = places;
    });
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.placesService.fetchPlaces().subscribe(() => {
      this.isLoading = false;
    });
  }

  onEdit(id: string) {
    console.log(id);
  }
  onDelete(id: string) {
    this.loadingController
      .create({
        message: 'Deleting place...',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.placesService.deletePlaceById(id).subscribe(() => {
          loadingEl.dismiss();
        });
      });
  }

  ngOnDestroy(): void {
    if (this.placeSub) {
      this.placeSub.unsubscribe();
    }
  }
}
