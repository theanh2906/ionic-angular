import { Component, OnInit } from '@angular/core';
import { PlacesService } from 'src/app/places/places.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, NavController } from '@ionic/angular';
import { PlaceLocation } from 'src/app/shared/pickers/location-picker/location-picker.component';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-new-offer',
  templateUrl: './new-offer.page.html',
  styleUrls: ['./new-offer.page.scss'],
})
export class NewOfferPage implements OnInit {
  form: FormGroup;
  constructor(
    private placesService: PlacesService,
    private navController: NavController,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.maxLength(180)],
      }),
      price: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(1)],
      }),
      dateFrom: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      dateTo: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      location: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
      image: new FormControl(null, {
        updateOn: 'blur',
      }),
    });
  }

  onCreateOffer = () => {
    if (!this.form.valid || !this.form.get('image').value) {
      return;
    }
    const formData = this.form.value;
    this.loadingController
      .create({
        message: 'Creating place...',
      })
      .then((loadingEl) => {
        loadingEl.present();
        this.placesService
          .uploadImage(this.form.get('image').value)
          .pipe(
            switchMap((uploadRes) =>
              this.placesService.addPlace(
                formData.title,
                formData.description,
                +formData.price,
                formData.dateFrom,
                formData.dateTo,
                formData.location,
                uploadRes.imageUrl
              )
            )
          )
          .subscribe(() => {
            loadingEl.dismiss();
            this.form.reset();
            this.navController.navigateBack('/places/tabs/offers');
          });
      });
  };

  onLocationPicked(location: PlaceLocation) {
    this.form.patchValue({ location });
  }

  onImagePicked(image: string | File) {
    let imageFile;
    if (typeof image === 'string') {
      try {
        imageFile = base64toBlob(
          image.replace('data:image/jpeg;base64,', ''),
          'image/jpeg'
        );
      } catch (err) {
        console.log(err);
        return;
      }
    } else {
      imageFile = image;
    }
    this.form.patchValue({ image: imageFile });
  }
}

const base64toBlob = (base64Data, contentType) => {
  contentType = contentType || '';
  const sliceSize = 1024;
  const byteCharacters = atob(base64Data);
  const bytesLength = byteCharacters.length;
  const slicesCount = Math.ceil(bytesLength / sliceSize);
  const byteArrays = new Array(slicesCount);

  for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
    const begin = sliceIndex * sliceSize;
    const end = Math.min(begin + sliceSize, bytesLength);

    const bytes = new Array(end - begin);
    for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
      bytes[i] = byteCharacters[offset].charCodeAt(0);
    }
    byteArrays[sliceIndex] = new Uint8Array(bytes);
  }
  return new Blob(byteArrays, { type: contentType });
};
