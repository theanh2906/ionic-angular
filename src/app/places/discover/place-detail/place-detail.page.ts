import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ActionSheetController,
  AlertController,
  LoadingController,
  ModalController,
  NavController,
} from '@ionic/angular';
import { Place, PlacesService } from 'src/app/places/places.service';
import { CreateBookingComponent } from 'src/app/bookings/create-booking/create-booking.component';
import { BookingsService } from 'src/app/bookings/bookings.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit, OnDestroy {
  place: Place;
  isBookable = false;
  isLoading = false;
  private placeSub: Subscription;

  constructor(
    private router: Router,
    private navController: NavController,
    private route: ActivatedRoute,
    private placesService: PlacesService,
    private modelController: ModalController,
    private actionSheetController: ActionSheetController,
    private bookingsService: BookingsService,
    private loadingController: LoadingController,
    private authService: AuthService,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      if (!params.placeId) {
        this.navController.navigateBack('/places/tabs/discover');
      }
      this.isLoading = true;
      let fetchUserId: string;
      this.authService.userId
        .pipe(
          take(1),
          switchMap((userId) => {
            if (!userId) {
              throw new Error('No user id found!');
            }
            fetchUserId = userId;
            return this.placesService.getPlaceById(params.placeId);
          })
        )
        .subscribe(
          (place) => {
            this.place = place;
            this.isBookable = place.userId !== fetchUserId;
            this.isLoading = false;
          },
          (error) => {
            this.alertController
              .create({
                header: 'An error occurred!',
                message: 'Place could not be fetch. Please try again later',
                buttons: [
                  {
                    text: 'OK',
                    handler: () => {
                      this.router.navigate(['/places/tabs/discover']);
                    },
                  },
                ],
              })
              .then((alertEl) => {
                alertEl.present();
              });
          }
        );
    });
  }

  onBookPlace() {
    this.actionSheetController
      .create({
        header: 'Choose an action',
        buttons: [
          {
            text: 'Select Date',
            handler: () => {
              this.openBookingModal('select');
            },
          },
          {
            text: 'Random Date',
            handler: () => {
              this.openBookingModal('random');
            },
          },
          {
            text: 'Cancel',
            role: 'cancel',
          },
        ],
      })
      .then((actionSheetEl) => actionSheetEl.present());
  }

  openBookingModal = (mode: 'select' | 'random') => {
    this.modelController
      .create({
        component: CreateBookingComponent,
        componentProps: { place: this.place, mode },
        id: 'bookModal',
      })
      .then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      })
      .then((resultData) => {
        if (resultData.role === 'confirm') {
          this.loadingController
            .create({
              message: 'Booking place...',
            })
            .then((loadingEl) => {
              loadingEl.present();
              this.bookingsService
                .addBooking(resultData.data.bookingData)
                .subscribe(() => {
                  loadingEl.dismiss();
                });
            });
        }
      });
  };

  ngOnDestroy(): void {
    if (this.placeSub) {
      this.placeSub.unsubscribe();
    }
  }
}
