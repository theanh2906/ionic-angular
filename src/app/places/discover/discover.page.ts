import { Component, OnDestroy, OnInit } from '@angular/core';
import { Place, PlacesService } from 'src/app/places/places.service';
import { MenuController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit, OnDestroy {
  places: Place[];
  listPlaces: Place[];
  relevantPlaces: Place[];
  isLoading = false;
  private filter = 'all';
  private placesSub: Subscription;

  constructor(
    private placesService: PlacesService,
    private menuController: MenuController,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.isLoading = true;
    this.placesSub = this.placesService.places.subscribe((places) => {
      this.places = places;
      this.relevantPlaces = this.places;
      this.isLoading = false;
    });
  }

  ionViewWillEnter() {
    this.isLoading = true;
    this.placesService.fetchPlaces().subscribe(() => {
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.placesSub) {
      this.placesSub.unsubscribe();
    }
  }

  onOpenMenu() {
    this.menuController.open('menu');
  }

  onFilterUpdate(filter: any) {
    this.authService.userId.pipe(take(1)).subscribe((userID) => {
      const isShown = (place) =>
        filter.detail.value === 'all' || place.userId !== userID;
      this.relevantPlaces = this.places.filter(isShown);
      this.filter = filter;
    });
  }
}
